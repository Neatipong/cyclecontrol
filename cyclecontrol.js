'use strict';

//require("dotenv").config();
 
const express = require('express')

var app = require('express')();

var http = require('http');  //.createServer(app);
var https = require('https');
const bodyParser = require('body-parser')
require('body-parser-xml')(bodyParser); 
var 	dt 		=require('./resource/datetime');
var  dbcnn 		=require('./resource/dbconnect');
const fs		=require('fs'); 
const config	=require('config')
const prdsrv	=require('./routes/posservice')
const xpand		=require('./routes/xpandpat')
const kbankapi	=require('./routes/kbankapi'); 
const pimg		=require('./routes/imgresize')
const catimg		=require('./routes/catimage')
const ucatimg		=require('./routes/usercatimage')
const xfer		=require('./routes/xferutil')
const eml		=require('./routes/mailer')
const xmlxp		=require('./routes/xpandxml')
const webserv	=require('./routes/webservice')
const menuController = require('./routes/menucontroller')
const xml2js = require('xml2js');
global.atob 	= require("atob");
var ip		=require('request');
var xl = require('excel4node');
const nodemailer = require('nodemailer');

const mnConfig = config.get('posmenu');
var  port 		= mnConfig.port ; 
global.inetgateway =mnConfig.inetgateway ; 

var options = {
			root: __dirname + '/public/',
			dotfiles: 'deny',
			headers: {
				'x-timestamp': Date.now(),
				'x-sent': true
			}
		  }; 
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');  //allow connection from any location!!! Importance
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

//app.use(express.json()); 
//app.use(express.urlencoded());
//------solve problem ... PayloadTooLargeError: request entity too large
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }))
app.use( express.static('public'))   //--allow static page to be display
app.use(bodyParser.xml({
	limit: '50mb',   // Reject payload bigger than 50 MB
	xmlParseOptions: {
	  normalize: true,     // Trim whitespace inside text nodes
	  normalizeTags: false, // Transform tags to lowercase
	  explicitArray: false // Only put nodes in array if >1
	}
  }));
 
 
app.get('/', (req, res) => res.sendFile("maincontrol.html", options, function (err) {
	if (err) {
	  res.send(err);
	} else {
	  console.log('Calling control');
	}
  }) 
)
  
//-----------Customer Scan BARCODE that have link
app.get('/S/:skcode',function(req,res){
    
	 var fname='./public/SERVICE/custhome.html';  //?? if member then memHome but How we can tell diff ???
     
  
    var skc =req.params.skcode;
    try {
		var ip = req.headers['x-forwarded-for'] || 
	     req.connection.remoteAddress || 
	     req.socket.remoteAddress ||
	     (req.connection.socket ? req.connection.socket.remoteAddress : null);
		   ip=ip.replace('::ffff:','');
		   
		   var data = fs.readFileSync( fname,'utf8');
	    	 
	    	 data=data.replace('@skcode',skc);
	    	 data=data.replace('@callip',ip);
	    	// data=data.replace('@custref',cust);
	    	 data=data.replace('@xpdname','serviceskcode.js');
	    	 res.status(200).send(data);
			 res.end(); 
			 
		   //------------------------
		      console.log(dt.myDateTime()+' Service  '+ip);
	 
    	
    }
    catch(err){
 	    res.status(404).send("Scan Product Return Error  "+err.message);
	    res.end();
     }
});
app.get('/payatstore', (req, res) => res.sendFile("/PAYMENT/pospayment.html", options, function (err) {
	if (err) {
	  res.send(err);
	} else {
	  console.log('Calling control');
	}
  }) 
)
app.get('/finance', (req, res) => res.sendFile("/financial.html", options, function (err) {
	if (err) {
	  res.send(err);
	} else {
	  console.log('Calling control');
	}
  }) 
)
app.get('/about', (req, res) => res.sendFile("aboutapp.html", options, function (err) {
		if (err) {
		  res.send(err);
		} else {
		  console.log('Calling control');
		}
	  }) 
	)
	
//-----------get BASE 64 Image of this SKCODE
app.get('/B/:skcode',function(req,res){
	  var  skc=req.params.skcode;
	  var qry= "select base64 from twdpim..PIMIMAGE where SKC ="+skc;
	 console.log(qry);
	  try {
	     	dbcnn.dbGetQuery(qry,function(resp){
	     		// console.log(resp);
	     		if(resp.dbcode==0&&resp.dbrows>0){
	     			var bimg=resp.dbitems[0].base64;
	     			res.status(200).send("data:image/jpeg;base64,"+ bimg);
	     		}
	     		else
	     			res.status(200).send('');
	     	});
	  }
	  catch(ex){
		    console.log(ex,nessage);
		  	res.status(200).send('') }; 
	  }
		  
     ) ;
//-----------get BASE 64 Image of this SKCODE
app.get('/CATIMG/:cat',function(req,res){
	var  cat=req.params.cat;
	cat=(cat+'00000000').slice(0,6);
	var qry= "select base64 from CATIMAGE where CATCODE ='"+ cat +"'";
   console.log(qry);
	try {
		   dbcnn.dbGetQuery(qry,function(resp){
			   // console.log(resp);
			   if(resp.dbcode==0&&resp.dbrows>0){
				   var bimg=resp.dbitems[0].base64;
				   res.status(200).send("data:image/jpeg;base64,"+ bimg);
			   }
			   else
				   res.status(200).send('');
		   });
	}
	catch(ex){
		  console.log(ex,nessage);
			res.status(200).send('') }; 
	}
		
   ) ;

//-----------Display Detail Page other than main page
app.get('/DPAGE',function(req,res){
    var fname='./public/detailpage.htm';
    try {
    	 var data = fs.readFileSync( fname,'utf8');
    	 res.status(200).send(data);
		 res.end(); 
    }
    catch(err){
 	    res.status(404).send("Can not Open File "+fname +"<br>"+err.message);
	    res.end();
     }
});
 
 //***********Expand file by CTLF
app.get('/CTLF',  function(req,res){
	var query = require('url').parse(req.url,true).query;
    var fname=query.fname;
    
    if((fname=='undefined')||(fname==null)){ 
	  res.status(200).send('NO fname defined in the parameter');
	  res.end();
	  return;
	}
  
    fname='./public/'+fname;
  //  console.log('Open file '+fname);
    
   var cdir = fname.substring(0,fname.lastIndexOf('/')+1);
  // console.log('directory '+cdir); 		
	 var rethtm='';
	 try {
		 var data = fs.readFileSync( fname,'utf8');
		    //add include File if any
		  var ps=data.indexOf('<!--*inc ');
		    if(ps>0){
		    	var es=data.indexOf('*-->');
		    	var inc =data.substring(ps+8,es);
		    	console.log('Include File '+ps+ ':'+es+' :' +inc);
		    	//--opne that file and Include
		    	 try {
		    		 var incf=cdir+inc.trim() ;
		    		 var dd=fs.readFileSync( incf,'utf8');
		    		 data=data.substring(0,ps)+dd+data.substring(es+4);
		    	 }
		    	 catch(ex) {
		    		 //--do nothing
		    		 console.log(ex.message);
		    	 } 
		    }
			res.status(200).send(data);
			res.end(); 
	 }
     catch(err){
 	    res.status(404).send("Can not Open File "+fname +"<br>"+err.message);
	    res.end();
     }
	}
  )
//-----------Display Detail Page other than main page
app.get('/EXTPIM',function(req,res){
    var fname='./public/extendpim.html';
    try {
    	 var data = fs.readFileSync( fname,'utf8');
    	 res.status(200).send(data);
		 res.end(); 
    }
    catch(err){
 	    res.status(404).send("Can not Open File "+fname +"<br>"+err.message);
	    res.end();
     }
});
app.get('/host',function(req,res){
	try { 

		 var ip=req.ip;
		  res.status(200).send("My IPaddress is "+ip);
	}
  catch(ex){
	   console.log(ex.message);
		res.status(200).send(ex.message);
     }
   }
 )
	
 //----Print QR Code of product to image base64
app.get('/qrcode/:skcode', function(req,res){
	try { 
		 var  skc = req.params.skcode;
		 var  url='http://twpim.com:81/S/'+skc;
		 console.log('req for QR '+url);	
	     qr64.genQR64(url,function(ret){  //return string of base64 image
	     res.status(200).send('<img src='+ret+'>');   
		 res.end();
		 });
	     }
     catch(ex){
	  	   console.log(ex.message);
	  		res.status(200).send(ex.message);
	       }
	}
)

//-----------multer Upload
var multer  =   require('multer');
const { cache } = require('sharp');
 
 var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, 'public/uploads');
  },
  filename: function (req, file, callback) {
    callback(null,  file.fieldname +'.jpg' );
   // callback(null,  'photo.jpg' );
  }
});

var upload = multer({ storage : storage}).single('userPhoto');

app.post('/upload',function(req,res){
   console.log("Try to Upload " +  req.body);
    try {
      upload(req,res,function(err) {
        if(err) {
          console.log(err)
            return res.end("Error uploading file.");
        }
       res.end("File is uploaded ");
      });
    }
  catch(ex){
    res.end("Error  " +ex.message);
  }
});
//------------------ 
 //***********change password for Internal User
app.get('/PASSW/:par',  function(req,res){
    try { 
    var para=atob(req.params.par);  //in the form USERID=xxxx&PASSW=YYYY
    var paras=para.split('&') ;
    if( (paras.length<2) ) {
    	res.status(404).send('Invalid Parameter');
    	res.end();
    	return;
    }
    var uname =paras[0].split('=')[1];
	var email =paras[1].split('=')[1];
	var expire =paras[2].split('=')[1];  //expire date 

	var td =new Date()
	var exp =new Date(expire)
	var diff =td-exp;   //date diff in second
	if(diff/(3600*1000) >  1){  //more than 1 Hour
		res.status(200).send("[This Change Password] page is out of Date");
		res.end();
	}

    var fname='./public/chpassw.htm' ;
  
    	  var data = fs.readFileSync( fname,'utf8'); 
    	  //--replace parameter ti data
    	  data=data.replace('^USERID^',uname);
    	  data=data.replace('^EMAIL^',email);
    	  res.status(200).send(data);
    	  res.end();
       }
	 catch(ex){
		  res.status(404).send("Can not Open File "+fname +"<br>"+ex.message);
		    res.end();
	 }
     
} );
app.post('/genExcelBigtoMail', async (req, res,next) => {
	let getemailpo = req.body.emailpo;
	let getemailto = req.body.emailto;
	let getemailcc = req.body.emailcc;
	let getemailtitle = req.body.emailtitle;
	let getemailtext = req.body.emailtext;
	let getemailuser = req.body.emailuser;
	let getemailpass = req.body.emailpass;
	let getemailfilename = req.body.emailfilename;

	function genexcel4node(){
		let dateexport = new Date().toISOString().replace(/:/g, '-').replace('.', '-');;
	
		wb.write(`public/assetExcel/${getemailfilename}_${dateexport}.xlsx`, function(err, stats) {
			if (err) {
				console.error(err);
				res.status(404).send(err);
			} else {

				
				const transporter = nodemailer.createTransport({
					service: 'hotmail',
					auth: {
					user: getemailuser, // your email
					pass: getemailpass              // your password
					}
				});
					// setup email data with unicode symbols
				const mailOptions = {
					from: getemailuser,              // sender
					to: getemailto,              // list of receivers
					subject: getemailtitle,            // Mail subject
					cc:getemailcc ,
					html: getemailtext,
					attachments: [ { filename: `${getemailfilename}_${dateexport}.xlsx`,
								  contentType: 'application/xlsx',
								  path: `public/assetExcel/${getemailfilename}_${dateexport}.xlsx` } ] 
				};

				transporter.sendMail(mailOptions, function (err, info) {
					if(err){
					console.log(err)	
					res.json({
						data: err,
						message: `sendmail error`,
						status: "error"
					  }); 
					}
					else{
					  res.json({
						data: info,
						message: `sendmail success`,
						status: "success"
					  }); 
					  fs.unlink(`public/assetExcel/${getemailfilename}_${dateexport}.xlsx`, function (err) {
						if (err){
						  console.log("Not have file");
						  
						}else{
						  console.log("File deleted");
						 
						}
						
						
					}); 
					}
				});
			}
			});
		
	}
	// Create a new instance of a Workbook class
	var getqrydata = await callsql(`exec twdpim..execPORequestHist_ListOrder 'getdetailmultiponum', null, null, '${getemailpo}' , null, null`) 
	//console.log(getqrydata);
	if(getqrydata.dbcode==0){
		var wb = new xl.Workbook();
	
		// Add Worksheets to the workbook
		var ws = wb.addWorksheet('Sheet 1');

		
		// Create a reusable style
		var styleP = wb.createStyle({
			fill: {
				type: 'pattern',
				patternType: 'solid',
				bgColor: 'violet',
				fgColor: 'violet',
			  }
		});
		var styleY = wb.createStyle({
			fill: {
				type: 'pattern',
				patternType: 'solid',
				bgColor: 'yellow',
				fgColor: 'yellow',
			  }
		});
		var styleG = wb.createStyle({
			fill: {
				type: 'pattern',
				patternType: 'solid',
				bgColor: 'green',
				fgColor: 'green',
			  }
		});
		
		// Set value of cell A1 to 100 as a number type styled with paramaters of style
		// ws.cell(1, 1).string('ลำดับ').style(style);
		ws.cell(1, 1).string('ลำดับ');
		ws.cell(1, 2).string('PO');
		ws.cell(1, 3).string('SKU');
		ws.cell(1, 4).string('BARCODE');
		ws.cell(1, 5).string('Bar8');
		ws.cell(1, 6).string('NAME');
		ws.cell(1, 7).string('PRICE IN VAT');
		ws.cell(1, 8).string('PRICE EX. VAT');
		ws.cell(1, 9).string('NORMAL PRICE');
		ws.cell(1, 10).string('DISCOUNT');
		ws.cell(1, 11).string('จำนวนใน PO').style(styleP);
		ws.cell(1, 12).string('สาขาแจ้งกลับสินค้าพร้อมส่ง').style(styleY);
		ws.cell(1, 13).string('Stock Avail').style(styleG);
		ws.cell(1, 14).string('สาขา');
		ws.cell(1, 15).string('Total In vat');
		ws.cell(1, 16).string('Total Ex vat');
		let getRowNum = getqrydata.dbrows;
		getqrydata.dbitems.forEach(function (e, i) {
			let useIndex=i+2;
			let ckIndex=i+1;
			ws.cell(useIndex, 1).number(e.SEQ);
			ws.cell(useIndex, 2).string(e.PO_NUM);
			ws.cell(useIndex, 3).number(e.SKU);
			ws.cell(useIndex, 4).string(e.Barcode);
			ws.cell(useIndex, 5).string(e.Bar8);
			ws.cell(useIndex, 6).string(e.PR_NAME);
			ws.cell(useIndex, 7).string(e.PriceInVat);
			ws.cell(useIndex, 8).string(e.PriceExVat);
			ws.cell(useIndex, 9).string(e.normalprice);
			ws.cell(useIndex, 10).string(e.Discount);
			ws.cell(useIndex, 11).string(e.ORG_Qty);
			ws.cell(useIndex, 12).string(e.Qty);
			ws.cell(useIndex, 13).number(e.StockAvail);
			ws.cell(useIndex, 14).string(e.stc);
			ws.cell(useIndex, 15).string(e.TotalInvat);
			ws.cell(useIndex, 16).string(e.TotalExVat);
		
			if(getRowNum==ckIndex){
				genexcel4node();
			}
		});
	}
	
});
app.post('/getaddressfromcid', (req, res,next) => {
	var getcid = req.body.cid;
	ip({
			method: 'POST',
			uri: 'http://10.77.1.6/directsaleapi/api/CustomerDS/GetCustomerByID',
			headers: {
				'Content-Type': 'multipart/form-data;'
			},
			form: {
				CustomerId: getcid
			}
		}, 
		(err, httpResponse, body) => {
			if(err){
				res.json({
					data: err,
					message: `err`,
					status: "0"
				}); 
			} else {
				res.json({
					data: JSON.parse(body),
					message: `message`,
					status: "1"
				}); 
			}
		}
	);
});

app.post('/datatodirectsale', (req, res,next) => {
	var datareq=JSON.stringify(req.body.feeddata);
	var fpsdlf = JSON.parse(datareq);
	ip({
			method: 'POST',
			uri: 'http://10.77.1.6/DirectSaleAPI/api/Quotation/AddQuotaion',
			headers: {
				'Content-Type': 'application/json;'
			},
			json: fpsdlf
		}, (err, httpResponse, body) => {
			if(err){
				res.json({
					data: err,
					message: `err`,
					status: "0"
				}); 
			} else {
				res.json({
					data: body,
					message: `message`,
					status: "1"
				}); 
			}
		}
	);
});

app.post('/CheckCredit', (req, res,next) => {
	let getCustomerID = req.body.cid;
	let getMMSCode = req.body.mmscode;
	ip({
			method: 'POST',
			uri: 'http://10.77.1.6/DirectSaleAPI/api/CustomerDS/CheckCredit',
			headers: {
				'Content-Type': 'multipart/form-data;'
			},
			form: {
				CustomerID: getCustomerID,
				MMSCode:getMMSCode
			}
		}, 
		(err, httpResponse, body) => {
			if(err){
				res.json({
					data: err,
					message: `err`,
					status: "0"
				}); 
			} else {
				res.json({
					data: JSON.parse(body),
					message: `message`,
					status: "1"
				}); 
			}
		}
	);
});


   app.post('/apixmltest',async (req, res,next) => {
	console.log(req.body);
	var getdata = req.body;
	res.status(200).send(getdata);
	res.end();
});

async function callsql(qry){
	return new Promise((res , rej) =>{
		 console.log(qry);
		dbcnn.dbGetQuery(qry,function(resp,){
			res(resp);
		});
	})
}
async function callapigetso(getponum){
	return new Promise((res , rej) =>{

			ip({
					method: 'POST',
					uri: 'http://10.77.1.6/DirectSaleAPI/api/Quotation/CheckQuotatoin',
					headers: {
						'Content-Type': 'application/json;'
					},
					json: {
						"poNumber": getponum
					  }
				}, (err, httpResponse, body) => {
					if(err){
						res("ERR");
					} else {
						res(body);
					}
				}
			);
		
	});
}
app.post('/apicpf',async (req, res,next) => {
 try{
	//console.log(req.body);
	var getdata = req.body;
	 var getFuncName = getdata.Request.$.RFCFuncName;
	 console.log(getFuncName)
	var dataxmltojson = [];
	 if(getFuncName=='sendOrder'){
		var getPLANT = getdata.Request.Structure.PLANT;// "SALE_NUM": "18895"
		var getSALE_NUM = getdata.Request.Structure.SALE_NUM;// "SALE_NUM": "18895"
		var getMESSAGE_ID = getdata.Request.Structure.MESSAGE_ID;// "MESSAGE_ID": "32",
		var getBUYER_NAME = getdata.Request.Structure.BUYER_NAME;//"BUYER_NAME": "CPFSAP",
		var getPO_NUM = getdata.Request.Structure.PO_NUM;//  "PO_NUM": "4802604337",
		var getPOISSUE_DATE = getdata.Request.Structure.POISSUE_DATE;// "POISSUE_DATE": "2010-05-14 03:34:53.0",
		var getTOTAL_AMT = getdata.Request.Structure.TOTAL_AMT;//"TOTAL_AMT": "63100.000",
		var getCURRENCY_CODE = getdata.Request.Structure.CURRENCY_CODE;// "CURRENCY_CODE": "THB",
		var getNAME = getdata.Request.Structure.NAME;// "NAME": "3004-BP-คลัง อสร."
		
		if(!getdata.Request.Table.length){
			var e = getdata.Request.Table;
			var getdataformxml = {
				seq: e.ITEM_NUM, 
				sku: e.BUYER_ITEMCODE,
				qty: e.QUANTITY,
				priceexvat: e.UNIT_PRICE
			};
			dataxmltojson.push(getdataformxml);
		}else{
			var i=0;
			for(i=0;i<getdata.Request.Table.length;i++){
				console.log(i);
				var e = getdata.Request.Table[i] ;
				var getdataformxml = {
					seq: e.ITEM_NUM, 
					sku: e.BUYER_ITEMCODE,
					qty: e.QUANTITY,
					priceexvat: e.UNIT_PRICE
				};
				
				dataxmltojson.push(getdataformxml);
			}
		}
		var getitem = JSON.stringify(dataxmltojson);
		var getqrydata = await callsql(`exec TWDPIM..execVerifyOrder_CPF '${getPLANT}','${getPO_NUM}','${getitem}'`) 
	
		var getitemmessage =[];
		var getsuccess ='';
		var getdescription ='';
		if(getqrydata.dbcode==0){
			if(getqrydata.dbrows > 0){
				getsuccess ='false';
				getdescription='Not Found SKU';
				for(i=0;i<getqrydata.dbrows;i++){
					var e = getqrydata.dbitems[i] ;
					var getdataforDB = {
						linenum: e.seq,
						soitem: i+1,
						message: {
							$: {
								"type": e.type
							},
							code: "",
							description: e.message
						}
					};
					getitemmessage.push(getdataforDB);
				}
			}else{
				getsuccess ='true';
				getitemmessage ='';
				getdescription='success';
			}
		}else{
			if(getqrydata.dbcode==888){
				getsuccess ='false';
				getitemmessage ='';
				getdescription=getqrydata.dbmessage;
			}else{
			getsuccess ='false';
			getitemmessage ='';
			getdescription='Failed to connect to Thai Watsadu database';
			}
		}

		var resData={
			"ipMessage": {
				"$": {
					"DOCID": getMESSAGE_ID,
					"name": "sendOrderReply",
					"version": "2.0"
				},
				"sale_order_num": getSALE_NUM,
				"messageid": getMESSAGE_ID,
				"purchase_order_num": getPO_NUM,
				"sendOrderReply": {
					"success": getsuccess,
					"conum": "",
					"message": {
						"$": {
							"type": "information"
						},
						"description": getdescription
					},
					"itemmessage": getitemmessage
				}
			}
		};
		var builder = new xml2js.Builder( {xmldec: { 'version': '1.0', 'encoding': 'UTF-8', 'standalone': false } } );
		var xml = builder.buildObject(resData);
		res.status(200).send(xml);
		res.end();
	
	 }else if(getFuncName =='getData'){

		var getPLANT = getdata.Request.Structure.PLANT;// "SALE_NUM": "18895"
		var getSALE_NUM = getdata.Request.Structure.SALE_NUM;// "SALE_NUM": "18895"
		var getMESSAGE_ID = getdata.Request.Structure.MESSAGE_ID;// "MESSAGE_ID": "32",
		var getBUYER_NAME = getdata.Request.Structure.BUYER_NAME;//"BUYER_NAME": "CPFSAP",
		var getPO_NUM = getdata.Request.Structure.PO_NUM;//  "PO_NUM": "4802604337",
		var getPOISSUE_DATE = getdata.Request.Structure.POISSUE_DATE;// "POISSUE_DATE": "2010-05-14 03:34:53.0",
		var getTOTAL_AMT = getdata.Request.Structure.TOTAL_AMT;//"TOTAL_AMT": "63100.000",
		var getCURRENCY_CODE = getdata.Request.Structure.CURRENCY_CODE;// "CURRENCY_CODE": "THB",
		var getNAME = getdata.Request.Structure.NAME;// "NAME": "3004-BP-คลัง อสร."

		var getsodata = await callapigetso(getPO_NUM);
		// {
		// 	status: '1',
		// 	message: 'success',
		// 	quotationid: 'QS320201015016',
		// 	ponumber: '22222'
		//   }
		//   { status: '0', message: 'nodata ponumber 123123123 in directsale' }
		var getconum = '';
		var getsuccess ='false';
		var getitemmessage ='';
		var getdescription=(getsodata.message)?getsodata.message:'Failed to connect directsale';
		if(getsodata.status=='1'){
			getsuccess ='true';
			getitemmessage ='';
			getdescription='success';
			getconum = getsodata.quotationid;
		}
		var resData={
			"ipMessage": {
				"$": {
					"DOCID": getMESSAGE_ID,
					"name": "getDataReply",
					"version": "2.0"
				},
				"messageid": getMESSAGE_ID,
				"sale_order_num": getSALE_NUM,
				"purchase_order_num": getPO_NUM,
				"getDataReply": {
					"success": getsuccess,
					"conum": getconum,
					"message": {
						"$": {
							"type": "information"
						},
						"code": '',
						"description": getdescription
					},
					"itemmessage": getitemmessage
				}
			}
		};
		
		 var builder = new xml2js.Builder( {xmldec: { 'version': '1.0', 'encoding': 'UTF-8', 'standalone': false } } );
		 var xml = builder.buildObject(resData);
		res.status(200).send(xml);
		res.end();
	 }

	}
	catch(ex){
		res.status(404).send(ex);
		res.end();
   }
});
app.post('/dssendupdatepo', async (req, res,next) => {
	let getfunc = req.body.func;
	let getpo_num = req.body.po_num;
	let getinvoice_no = req.body.invoice_no;
	let getticket = req.body.ticket;
	let getdlvdtl = req.body.dlvdtl;
	let getsku = req.body.sku;
	let getcn = req.body.cn;
	let getqty = req.body.qty;
	if(!getpo_num){
		res.status(400).json({
			data: "กรุณาระบุเลข PO",
			message: `กรุณาระบุเลข PO`,
			status: "0"
		}); ;
		res.end();
		return
	}

	switch(getfunc) {
		case "update_invoice_ticket":
			if(!getinvoice_no){
				res.status(400).json({
					data: "กรุณาระบุ invoice",
					message: `กรุณาระบุ invoice`,
					status: "0"
				}); 
				res.end(); 
				return
			}
			if(!getticket){
				res.status(400).json({
					data: "กรุณาระบุ ticket",
					message: `กรุณาระบุ ticket`,
					status: "0"
				}); 
				res.end(); 
				return
			}
			var getqrydata = await callsql(`exec twdpim..execPORequestHistory 'updinvoice_ticket', '${getpo_num}', null, null, null, null, null, null, '{"invoice_no":"${getinvoice_no}","ticket":"${getticket}"}'`); 
		  break;
		case "update_ticket_STS":
			if(!getticket){
				res.status(400).json({
					data: "กรุณาระบุ ticket",
					message: `กรุณาระบุ ticket`,
					status: "0"
				}); 
				res.end(); 
				return
			}
			var getqrydata = await callsql(`exec twdpim..execPORequestHistory 'updticket_STS', '${getpo_num}', null, null, null, null, null, null, '{"ticket":"${getticket}"}'`); 
		  break;
		case "update_delivery":
			if(!getdlvdtl){
				res.status(400).json({
					data: "กรุณาระบุ delivery",
					message: `กรุณาระบุ delivery`,
					status: "0"
				}); 
				res.end(); 
				return
			}
			var getqrydata = await callsql(`exec twdpim..execPORequestHistory 'upddelivery_det', '${getpo_num}', null, null, null, null, null, null, '{"dlvdtl":"${getdlvdtl}"}'`); 
		  break;
		case "update_CN":
			if(!getsku){
				res.status(400).json({
					data: "กรุณาระบุ sku",
					message: `กรุณาระบุ sku`,
					status: "0"
				}); 
				res.end(); 
				return
			}
			if(!getcn){
				res.status(400).json({
					data: "กรุณาระบุ cn",
					message: `กรุณาระบุ cn`,
					status: "0"
				}); 
				res.end(); 
				return
			}
			if(!getqty){
				res.status(400).json({
					data: "กรุณาระบุ qty",
					message: `กรุณาระบุ qty`,
					status: "0"
				}); 
				res.end(); 
				return
			}
			var getqrydata = await callsql(`exec twdpim..execPORequestHist_ListOrder 'update_CN', '${getpo_num}', null, null, null, null, '{"sku": "${getsku}", "cn":"${getcn}", "qty": "${getqty}"}'`); 
		  break;
		default:
			res.status(400).json({
				data: "กรุณาระบุ function ให้ถูกต้อง",
				message: 'กรุณาระบุ function ให้ถูกต้อง',
				status: "0"
			});
			res.end(); 
	  }
	
	  
	if(getqrydata.dbcode==0){
		res.status(200).json({
			data: getqrydata.dbitems,
			message: `success`,
			status: "1"
		});
		res.end(); 
	
	}else{
		res.status(400).json({
			data: getqrydata,
			message: 'DB Error',
			status: "0"
		});
		res.end(); 
	}
});
app.get("/contentpim/:imagepim/:subdept/:content", function(req, res) {
	var imagepim = req.params.imagepim;
	var subdept = req.params.subdept;
	var content = req.params.content;
	ip.defaults({ encoding: null }).get(`http://10.77.1.15/TWDPIM/web/${imagepim}/${subdept}/${content}`, function (error, response, body) {
		   if (!error && response.statusCode == 200) {
	  var type = response.headers["content-type"];
	  var img = Buffer.from(body);
			   res.writeHead(200, {
				   'Content-Type': type,
				   'Content-Length': img.length
			   });
			   res.end(img);
		   }else{
			   res.end(null);
		   }
	   });
   });

app.post("/Orders/Tracking", (req, res, next) => {
	let apikeys = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwidXNlc5hbWUiOiJKb2huIERvZSIsImlzcyI6InFXVFIwMDlQZWdldzI2MHdka1RUZ0lrUWtiRlhKcHZlIn0.smiNJwrI0tI9sT8_EonwL0LcKWBvTOriuOcWtUZzNzE';
	let token = req.header('Authorization');
	let jsnres;
	let pdata;
	let reqoption = {
		method: "post",
		headers: req.headers,
		body: req.body
	}
	
	if (token != apikeys) {
		jsnres = {
			errorMessage: 'Unauthorized token.',
			errorCode: "",
			systemDateTime: `${new Date().toISOString()}`
		};

		pdata = {
			qry: `exec CLOUDPOS..api_saveAPILog 'updatesorderstatus', '', '${JSON.stringify(reqoption)}', '${JSON.stringify(jsnres)}'`
		};
		dbcnn.execute(pdata,function(resp){
			res.status(401).send(jsnres);
			res.end();
		});
	} else {
		let salesource = req.body.saleSource;
		let ref = req.body.orderNo;
		let statuscode = req.body.statusCode;
		let trackingdet = req.body.trackingDetails;
		if(!salesource || !ref || !statuscode || !trackingdet) {
			let msg = '';
			if(!ref) msg += 'Order number, ';
			if(!salesource) msg += 'Salesource, ';
			if(!statuscode) msg += 'Status code, ';
			if(!trackingdet) msg += 'Tracking details, ';
			msg = msg.substring(0, msg.length - 2);
			jsnres = {
				errorMessage: `${msg} must not be empty.`,
				errorCode: "",
				systemDateTime: `${new Date().toISOString()}`
			};

			pdata = {
				qry: `exec CLOUDPOS..api_saveAPILog 'updatesorderstatus', '', '${JSON.stringify(reqoption)}', '${JSON.stringify(jsnres)}'`
			};
			dbcnn.execute(pdata,function(resp){
				res.status(412).send(jsnres);
				res.end();
			});
		} else {
			pdata = {
				qry: `exec CLOUDPOS..updateOrdersTrackingStatus '${ref}', '${salesource}', '${statuscode}', '${trackingdet}'`
			}
			dbcnn.execute(pdata,function(resp){
				if(resp.dbcode==0){
					jsnres = {
						errorMessage: "",
						errorCode: "",
						systemDateTime: `${new Date().toISOString()}`
					};

					let pdata = {
						qry: `exec CLOUDPOS..api_saveAPILog 'updatesorderstatus', '', '${JSON.stringify(reqoption)}', '${JSON.stringify(jsnres)}'`
					};
					dbcnn.execute(pdata,function(resp){
						res.status(200).send(jsnres);
						res.end();
					});
				} else {
					jsnres = {
						errorMessage: resp.dbmessage,
						errorCode: "",
						systemDateTime: `${new Date().toISOString()}`
					};

					let pdata = {
						qry: `exec CLOUDPOS..api_saveAPILog 'updatesorderstatus', '', '${JSON.stringify(reqoption)}', '${JSON.stringify(jsnres)}'`
					};
					dbcnn.execute(pdata,function(resp){
						res.status(404).send(jsnres);
						res.end();
					});
				}
			});
		}
	}
});

app.post("/genReportCustomer", (req, res, next) => {
	let pdata = {
		qry: `exec TWDPIM..Rep_CustomerOnWeb`
	}
	dbcnn.execute(pdata,function(resp){
		if(resp.dbcode == 0){
			let table='', header='', body='';
			let keys = Object.keys(resp.dbitems[0]);
			keys.map((itm) => {header += `<th>${itm}</th>`});
			header = `<thead><tr>${header}</tr></thead>`;

			resp.dbitems.map((itm, idx, arr) => {
				let det = '';
				let values = Object.values(itm);
				values.map((itm) => {det += `<td>${itm}</td>`});
				det = `<tr>${det}</tr>`;
				body += det;

				if((arr.length) - 1 == idx){
					body = `<tbody>${body}</tbody>`;
					table += `${header + body}`
					let jsnres = {
						status: "success",
						data: table
					};
					res.status(200).send(jsnres);
					res.end();
				}
			})
		} else {
			let jsnres = {
				status: resp.dbmessage,
				data: ''
			};
			res.status(404).send(jsnres);
			res.end();
		}
	});
});

app.get("/calldeliveryRate", function(req, res) { // user route
	var saleSource = req.query.saleSource;
	var deliveryFromStore = req.query.deliveryFromStore;
	var deliveryType = req.query.deliveryType;
	var shipToPostalCode = req.query.shipToPostalCode;
	var shipToLatitude = req.query.shipToLatitude;
	var shipToLongitude = req.query.shipToLongitude;
	var packageWeight = req.query.packageWeight;
	var totalprice = req.query.totalprice;
	var options = {
		uri: 'https://uat-apis.thaiwatsadu.com/sts/api/v1/th/Delivery/DeliveryCharge',
		method: 'GET',
		headers:{
            Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJpc3MiOiJUTWZmTkE1bkxlajVaTFBWbXB2OUR1S0tta2NJa1k3NyJ9.sHJXmnpuKPrNORrrPv-az5kf-HsR97x9Un3Ph0Yq8xo'            
       	},
		json: {
			"saleSource":saleSource,
			"deliveryFromStore":deliveryFromStore,
			"deliveryType":deliveryType,
			"shipToPostalCode":shipToPostalCode,
			"shipToLatitude":shipToLatitude,
			"shipToLongitude":shipToLongitude,
			"packageWeight":packageWeight,
			"totalprice":totalprice
		   }
	  };
	  ip(options, (req, ress) => {
			res.json(ress.body);
	  });
});
//const webserv	=require('./routes/webservice');  
//app.use(express.json())

app.use('/XPAND',xpand)
app.use('/POSSRV',prdsrv)
app.use('/IMAGE',pimg)
app.use('/CATIMG',catimg)
app.use('/USRCATIMG',ucatimg)
app.use('/XPDXML',xmlxp)
app.use('/XFER',xfer)
app.use('/EMAIL',eml)
app.use('/KBANK',kbankapi)
app.use('/WEBCALL',webserv)
app.use('/MenuController', menuController);




// var httpsOptions = {
//     key:  fs.readFileSync('CERT/upfdev_com.key'),
//     cert: fs.readFileSync('CERT/upfdev_com.crt')
// };
 

http.createServer(app).listen(96,
	function(){
		  console.log(dt.myDateTime() +' listening on *:96');
	    });
	 
// https.createServer(httpsOptions, app).listen(433,
// 	function(){
// 		console.log('https listening on *:433');
// 	  });
	

//http.listen(81, function(){
 // console.log('listening on *:81');
// });


//app.use('/CART',cserv)
//app.listen(port, () => console.log(dt.myDateTime() +` Start app listening on port ${port}!`)) // use ` to worjk with $
//
 
