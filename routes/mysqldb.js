var express = require('express')
var router = express.Router()
var bodyParser = require('body-parser')
var dt = require('../resource/datetime');


// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  console.log('call MYSQLDB at Time: ', dt.myDateTime())
  next()
})

//router.use(express.bodyParser());
router.use(express.json());

router.get('/CTLF/:name', function (req, res, next) {
	  var options = {
		root: __dirname + '/public/',
		dotfiles: 'deny',
		headers: {
			'x-timestamp': Date.now(),
			'x-sent': true
		}
	  };

	  var fileName = req.params.name;
	  res.sendFile(fileName, options, function (err) {
		if (err) {
		  next(err);
		} else {
		  console.log('Sent:', fileName);
		}
	  });
});

var ms 	= require('../resource/mysql');
var xpd = require('../resource/xpand')

router.post('/QUERY' , function (req, res) {
	console.log("Request Body...");
	console.log(req.body);
	if(!req.body.query){
		res.status(400).json({
				error:{ 
					message:'query is not defined'  ,
					code:99
				},
				resp:null,
				time:dt.myDateTime()
			})
		return; 
	}
     var qry=req.body.query;
     	ms.msGetQuery(qry,function(resp){
     		res.status(200).json({
				error:{ 
					message:resp.dbmessage  ,
					code:resp.dbcode
				   },
				resp:resp,
				time:dt.myDateTime()
				});
     	});
		
				
		//console.log("Query "+qry)  ;
});
router.post('/XPDFILE' , function (req, res) {
	//console.log(req.body);
	if(!req.body.patname){
		    res.send("<h3>No pattern file  specified</h3>");
		    return; 
	  }
	//-----------------Read pattern 
	var fname=req.body.patname;
	 fs.readFile(fname,'utf8',function read(err, data) {
		    if (err) {
		        res.send('Pattern File'+ fname +' not found');
		        return;
		    }
		   res.send( data);
		   return;
		});
	//----------------------
     var hdrs=req.body.hdr;
     var qrys=req.body.qry;
     var bdys=req.body.bdy;
     var ends=req.body.end;
     var errs=req.body.erx;
    // console.log(qrys);

     ms.msGetQuery(qrys, function(resp){
    	// console.log(resp);
    	// return;
    	 
 	    var emsg=resp["dbmessage"];
	    var ecde=resp["dbcode"];
		var ret =resp["dbitems"];  //----result set return
		//console.log(ret);
		//return;
    	 if(ecde>0){		
				var html=hdrs + errs.replace("@error",emsg) +ends;
				res.send(html);
				}
		else {
			//--------SEnd result set to Xpand
			ctls={hdr:hdrs,
				  bdy:bdys,
				  erx:errs,
				  end:ends}
			
			xpd.expandPat(ctls,ret,function(htm){ //control string  + result set
				res.send(htm);
			});
		 
		  }
     	});
     
     });

router.post('/XPAND' , function (req, res) {
	//console.log(req.body);
	if(!req.body.qry){
		    res.send("No Query specified");
		    return; 
	  }
     var hdrs=req.body.hdr;
     var qrys=req.body.qry;
     var bdys=req.body.bdy;
     var ends=req.body.end;
     var errs=req.body.erx;
    // console.log(qrys);

     ms.msGetQuery(qrys, function(resp){
    	// console.log(resp);
    	// return;
    	 
 	    var emsg=resp["dbmessage"];
	    var ecde=resp["dbcode"];
		var ret =resp["dbitems"];  //----result set return
		//console.log(ret);
		//return;
    	 if(ecde>0){		
				var html=hdrs + errs.replace("@error",emsg) +ends;
				res.send(html);
				}
		else {
			//--------SEnd result set to Xpand
			ctls={hdr:hdrs,
				  bdy:bdys,
				  erx:errs,
				  end:ends}
			
			xpd.expandPat(ctls,ret,function(htm){ //control string  + result set
				res.send(htm);
			});
		 
		  }
     	});
     
     });

module.exports = router
