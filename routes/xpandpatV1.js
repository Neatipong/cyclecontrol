/**
 * http://usejsdoc.org/
 */
var express 	= require('express')
var router 		= express.Router()
var bodyParser 	= require('body-parser')
var dt 			= require('../resource/datetime');
var fs 			=require("fs");
var ms 			= require('../resource/mssql');
//var Step		= require('step');  //dont work for  sync
 

router.use(function timeLog (req, res, next) {
  console.log('call MsSQLDB at Time: ', dt.myDateTime())
  next()
})


router.use(express.json());
 
var xpd = require('../resource/xpand');

router.post('/' , function (req, res) {
	 console.log(req.body);
	if(!req.body.patname){
		    res.send("<h3>No Pattern Filespecified</h3>");
		    return; 
	  }
	//-------------------- 
	var fname	= 	"."+ req.body.patname;
	
	
	//console.log(para);
	
	
	fs.readFile(fname,'utf8',function(err,data){
		if(err){
		//	console.log(err);
			res.send('<h3>Pattern File '+fname +' not Found</h3>');
			return;
		}
		else {
			//res.send(data);
			//--eliminate remark
			data=removeRemark(data);
			
			try { 
			var hdrs	="";
			var bdys	="";
			var qrys	="";
			var ends 	="";
			var errs	="";
			var cmps	="";
			var tabs	=""; 
			var pres	=""; 
			 //-------Replace parameter 
			  var para	=	 req.body.para;
			 for(key in para){
				 var val =para[key];
				 var rp  =RegExp('@'+key,'g');
				 data=data.replace(rp,val);
				}
			 
			 //----------------------
			var lns=data.split("$");
			for(var n=0;n<lns.length;n++)
				{
				var str =lns[n];
				if(str.substring(0,3)=="HDR" )hdrs=str.substring(4);
				if(str.substring(0,3)=="SQL" )qrys=str.substring(4);
				if(str.substring(0,3)=="BDY" )bdys=str.substring(4);
				if(str.substring(0,3)=="END" )ends=str.substring(4);
				if(str.substring(0,3)=="ERX" )errs=str.substring(4);
				if(str.substring(0,3)=="TAB" )tabs=str.substring(4);
				if(str.substring(0,3)=="PRE" )pres=str.substring(4);
				}
			
			//---!!!!! evaluate PRE Command>>?? it must be Synchroniz!!!!
		
			 if(pres > ""){
				 var pset=getSql(pres);
				 console.log(pset);   
			 }
			/// while(wait===0); // wait forever, it block other operation
		
			
			 console.log("wait pass");
			 //------------------------------
			  ctls={hdr:hdrs,
					  bdy:bdys,
					  erx:errs,
					  qry:qrys,
					  cmp:cmps,
					  end:ends}
				
				xpd.expandSql(ctls,function(htm){ //control string  + result set
					res.send(htm);
				});
	
			}
			catch(ex){
				res.send(ex.message);
			}
		  }
	     }) ;//---read pattern file sussecc
 
     });

function removeRemark(data){
	var ret="";
	var strs =data.split("\n");
	for(var i =0;i < strs.length; i++){
			var x =strs[i].trim();
			if (x.substr(0,2)!="//")ret = ret+ x +"\n"; 
	 }
	return(ret);
	
} 
//-----------------not work as expect 
function getSql(cmd){
	    var ret;
	    var async = require('async');
	    async.series([
	 /*       function (callback) {  
	            console.log('First Step --> ');
	            callback(null,'7');
	        },
	        function (callback) {
	            console.log('Second Step --> ');
	            callback(null,'p');
	        },
	        */
	        function(callback){
	            ms.msGetQuery(cmd, function(ret){
	                callback(null,ret);
	            });
	        }
	    ],
	    function (err, result) {
	     console.log(result);
	     ret=result;
	    });
	 return (ret);
}


module.exports = router