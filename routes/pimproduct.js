﻿var category = '01';
var curNode = 1;
//var waitgif="<image src='./images/wait.gif'>";

function pageReady() {
	openNode(1);

	$('#mainDiv').mouseleave(function () {
		$('#prodDiv').hide();
	});
}

function keyPMPress(ev) {
	if (ev.keyCode != 13) return;

	try {
		var pname = $('#inptxt').val();
		pname = pname.trim();
		$('#dispDiv').html(waitgif);
		var pat = "./PRODUCT/pimsearch.txt";
		var par = {
			"catcode": "0101",
			"srctxt": pname
		};

		ajaxExpTab(pat, par,
			function (bres, ret) {
				if (bres) {
					//-------Add object to wobj 
					$('#dispDiv').html(ret);
				} else
					$('#dispDiv').html('<b>ERROR..</b><br>' + ret);
			});

	} catch (ex) {
		alert(ex.message);
	}

}
//--Function New Product
function showProductX(node) {
	try {

		//  category=cat;
		$('#prodDiv').hide();
		$('#dispDiv').html(waitgif);
		var pat = "./PRODUCT/pimproduct.txt";
		var par = {
			"catnode": node
		};

		ajaxExpTab(pat, par,
			function (bres, ret) {
				if (bres) {
					//-------Add object to wobj 
					$('#dispDiv').html(ret);
				} else
					$('#dispDiv').html("<b>ERROR..</b><br>" + ret);
			});
	} catch (ex) {
		alert(ex.message);
	}
}

function createQR() {
	//alert('Call for detail');
	// var node =curNode;
	if ($("#dataTab").length <= 0) return;

	// ---create new tableto replace  content in prodDiv
	try {
		var htm = "<table id ='imageTab' class='table'>";
		htm += "<thead><th>Image<th>Name<th>QRCode</thead>";
		$('#dataTab tr:gt(0)').each(function () {
			var skc = $(this).find('td:eq(1)').text();
			skc = skc.trim();
			var nme = $(this).find('td:eq(2)').text();
			htm += "<td id='B" + skc + "'> IMG </td><td>" + skc + "</td><td>" +
				nme + " </td><td id='Q" + skc + "'>QRCODE</td><tr>";
		});
		htm += "</table>";
		$('#dispDiv').html(htm);
		setTimeout(createTQR(), 500); //delay  abit to document to settle
	}
	/**
		try{
			$('#dispDiv').html(waitgif);

		    var pdata={
		    		patname:"./PRODUCT/pimQR.txt",
		    		para:{catnode:node}
		    	}
		    
		    ajaxXpandFile(pdata,function(isok,data){
		    	 if(isok){
		    		 $('#dispDiv').html(data);
		    		 //--create QE Code
		    		  createTQR();
		    	     }
		    
		    	     
		    	 else 
		    		 $('#dispDiv').html("<b>ERROR..</b><br>"+data);  
		       })

		 
		}
		**/
	catch (ex) {
		alert(ex.message);
	}

	//------------- 
	function createTQR() {
		$('#imageTab tr:gt(0)').each(function () {
			//$('.qrDiv').each(function(){
			var sk = $(this).find('td:eq(1)').text();
			var url = '/qrcode/' + sk;
			//alert(url); 
			$.get(url, function (data) {
				//htm="<img src='"+ret +"'>";
				$('#Q' + sk).html(sk + '<br>' + data);
			});

			var url2 = '/B/' + sk;
			$.get(url2, function (data) {
				//alert(data);
				$('#B' + sk).html("<img src ='" + data + "' height='140px'>");
			});

		});
	}
	//-------------
}

//--------------------
function showProduct(node) {
	//alert('Call for detail');
	curNode = node;
	$('#prodDiv').hide();
	try {
		$('#dispDiv').html(waitgif);

		var pdata = {
			patname: "./PRODUCT/pimproduct.txt",
			para: {
				catnode: node
			}
		}

		ajaxXpandFile(pdata, function (isok, data) {
			if (isok)
				$('#dispDiv').html(data);
			else
				$('#dispDiv').html("<b>ERROR..</b><br>" + data);
		})


	} catch (ex) {
		alert(ex.message);
	}
}

function showDetail() {
	var skc = $(cobj).fimd('td:eq(1)').text();
	url = "/DPAGE?" + btoa("fname=PRODUCT/pimbyskcode.js&skcode=" + skc);
	window.open(url, 'DETAIL');
}

var navtab = "<ul class='nav nav-pills' role='tablist'> ";

function openNode(nd) {
	var sUrl = "/POSSRV/CATNODE/" + nd;
	$.get(sUrl, function (ret) {
		$('#catnavDiv').html(ret);
	});
	if (nd > 1) showProduct(nd); //--dont wiat for CatNave to finish
}

function prodInfo(skc) { //---product information at PIM
	try {
		openDetail("./PRODUCT/pimbyskcode.js&skcode=" + skc);
	} catch (ex) {
		alert(ex.message);
	}
}
//============
function hideDiv() {
	$('#prodDiv').hide();
}
var cobj = null;

function menuOver(obj) {
	cobj = obj;
	var prc = $(obj).find("td:eq(6)").text();
	var imgs = $(obj).find("td:eq(4)").text();
	var nme = $(obj).find("td:eq(2)").text();
	var skc = $(obj).find("td:eq(1)").text();
	var img = imgs.split("|")[1];
	skc = skc.trim();
	$('.card-text').html(skc + ".jpg<br>" + nme + "<br><b>ราคา " + prc + " บาท</b>");
	//getImage(skc);  //---seem be to slower than direct image
	//$('#imgDiv').html("<img src='http://vpn.upfront.co.th/TWDPIM/web/"+ img+  "' width='100%'>"); // on upfront Ubuntu
	//$('#imgDiv').html("<img src='http://upfrontpim.net/IMAGE/"+ skc +  ".jpg width='100%'>");
	// $('#imgDiv').html("<img src='http://vpn.upfront.co.th/POSIMAGE/"+ skc +".jpg'  width='100%'>");

	//	$('#imgDiv').html("<img src='http://upfrontpim.net:81/I/"+ skc +".jpg'  width='100%'>");
	// base 64 is slower that direct to file but not significant
	var url = "/B/" + skc;
	$.get(url, function (data) {
		$('#imgDiv').html(" <img src='" + data + "'  width='100%'  >");
	})

	var pos = $(obj).position();
	var mpos = $('#dispDiv').position();
	var tpos = $('#dataTab').position();
	$('#prodDiv').css('top', mpos.top + pos.top);
	$('#prodDiv').css('left', mpos.left + pos.left);
	$('#prodDiv').show();
}

function showDetail() {
	var skc = $(cobj).find("td:eq(1)").text();
	try {
		openDetail("fname=PRODUCT/pimbyskcode.js&skcode=" + skc);
	} catch (ex) {
		alert(ex.message);
	}
}

function copyImage() {
	if (confirm('Transfer these Images to CLD?') == 0) return;
	nrow = $('#dataTab tr:last').index();
	nrow += 1;
	try {
		xferImage(1);
	} catch (ex) {
		alert(ex.message);
	}
}

function xferImage(rwn) {
	if (rwn > nrow) return;
	try {
		var thisobj = $('#dataTab tr:eq(' + rwn + ')');
		var imgurl = $(thisobj).find('td:eq(4)').text();
		var skc = $(thisobj).find('td:eq(1)').text();
		var url = imgurl.split('|');
		if (url.length <= 1) {
			$(thisobj).find('td:eq(7)').html('NO Image');
			xferImage(++rwn);
		} else {
			var iurl = 'http://ubu.upfront.co.th/TWDPIM/web/' + url[1];

			var surl = '?skc=' + skc + '&imgurl=' + iurl;
			var url = "/XFER/IMAGE/" + surl;
			$.get(url, function (data) {
				$(thisobj).find('td:eq(7)').html(data);
				xferImage(++rwn);
			});
		}
	} catch (ex) {
		alert(ex.message);
	}
}

/**********
$HDR

<style>

.wraptd {
white-space: nowrap; 
	text-overflow:ellipsis; 
	overflow: hidden;
	max-width:200px;
}
#dataTab tr:hover{
	background-color:silver;
	cursor:pointer;
}
.card:hover{
	background-color:silver;
	cursor:pointer;
}

div.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
}



</style>
<div class='container-fulid'>
<br>

<div class='row'>

<div id ='catnavDiv' class='col col-md-6'> 
CATNODE Xpansion
</div>

<div class='col col-md-6'> 
<b>Search For:</b>
<input onkeypress ='javascript:keyPMPress(event)' type='text' id='inptxt' value =' '>
<a href ="javascript:createQR()" class='btn btn-primary pull-right'>Create QRCODe</a>
<a href ="javascript:copyImage()" class='btn btn-info pull-right'>Copy Image to DB</a> 
</div>

</div>

<div class='row'>

<div  id ='mainDiv' class='col col-md-12'>
<h3>ENRICHED PRODUCT @ TWDPIM</h3>
<h4>#summ</h4>

<div id ='dispDiv'>

<table id ='dataTab' style='width:98%'>
 <thead><th>Prcode<th>SkUcode<th>Name<th>Brand<th>image<th>LastUpdate<th>Ret Price</thead>
$SQL
//http://ubu.upfront.co.th/TWDSERVICE/mysqlService.php^
use.MYSQL^
Select prCode
, product.skCode as skCode
,prNameTH
,brand
,images
,product.lastupdate
,twdNormalPrice as price
from twdpim.product join twdpim.price on product.skcode =price.skCode
where enrich='Y'
 LIMIT 100
$BDY
<tr onmouseover ="javascript:menuOver(this)">
//<tr onclick ="javascript:menuOver(this)">
<td>:00<td>:01<td class='wraptd'>:02<td>:03<td>:04 
<td>:05<td align='right'>:06</tr>
$END
</table>

</div>
Note :Data from upfrontpim.net
</div>


 <div class="card" style=" width:24rem;display:none;z-index:4;
  		background-color:white;position:absolute;"
		id='prodDiv' onclick="javascript:showDetail()" > 
		<figure class="card card-product">
		<div id='imgDiv' class="card-img-top"> 
		</div>
	    <p class="card-text"></p>
 </div>

</div>

$TAB
|XID #summ
|HDR
|SQL
//http://ubu.upfront.co.th/TWDSERVICE/mysqlService.php^
use.MYSQL^
Select  sum(case when enrich='Y' then 1 else 0 end) as ENC
, count(*) as  cnt
from Twdpim.product 
|BDY
ALL Product Count <b>:01</b> Enriched PIM <b>:00</b>
|END

|ERX
ERROR @error
$xxSQL
 
http://ubu.upfront.co.th/TWDSERVICE/mysqlService.php^
http://upfrontpim.net/TWDSERVICE/mysqlService.php^
http://142.11.38.38/SQLSERVICE/curlService.php"
http://ubu.upfront.co.th/TWDSERVICE/mysqlService.php^
//command in musql
Select skCode,prCode,prNameTH,images,lastUpdate 
from twd.product LIMIT 100
//END****/