var express = require("express");

var router = express.Router();
var dt = require("../resource/datetime");

router.use(function timeLog(req, res, next) {
    console.log("Menu Controller : ", dt.myDateTime());
    next();
  });

  
router.get('/menu', (req, res) => {
    let menu = '';
    let profileLists = req.query.profileLists;
    if (profileLists.includes('1101') || profileLists.includes('1102') ||
        profileLists.includes('1103') || profileLists.includes('1104')) {
          menu += `<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                          aria-expanded="false">
                          กำหนด Location<span class="caret"></span></a>

                      <ul class="dropdown-menu">`;
          menu += (profileLists.includes('1104')) ? `<li><a href="javascript:verifyLocation()">ตรวจสอบ Location</a></li>` : '';
          menu += (profileLists.includes('1101')) ? `<li><a href="javascript:importLocation()">Import Location</a></li>` : '';
          menu += (profileLists.includes('1102')) ? `<li><a href="javascript:copyLocation2DB()">ดึง Location จาก MMS</a></li>` : '';
          menu += (profileLists.includes('1103')) ? `<li><a href="javascript:copyLocation2MMS()">ส่ง Location แทนที่ MMS</a></li>` : '';
          menu += `</ul></li>`;
    }

    if (profileLists.includes('1107') || profileLists.includes('1108') ||
        profileLists.includes('1106') || profileLists.includes('1129')) {
          menu += `<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                          aria-expanded="false">
                          กำหนดปฏิทินการนับ<span class="caret"></span></a>

                      <ul class="dropdown-menu">`;
          menu += (profileLists.includes('1107')) ? `<li><a href="javascript:cntSchedule()">สร้างชุดการนับ</a></li>` : '';  
          menu += (profileLists.includes('1108')) ? `<li><a href="javascript:cntCalendar()">ปฏิทินการนับ</a></li>` : '';
          menu += (profileLists.includes('1106')) ? `<li><a href="javascript:stdOpenClose()">เปิด-ปิดเวลานับ</a></li>` : '';
          menu += (profileLists.includes('1129')) ? `<li><a href="javascript:reasonCnt()">เหตุผลการนับ</a></li>` : '';          
          menu += `</ul></li>`;
    }

    if (profileLists.includes('1112')) {
      menu += `<li><a href="javascript:dashboardCount()">Dashboard Count</a></li>`;
    }

    if (profileLists.includes('1113') || profileLists.includes('1114') || 
        profileLists.includes('1115') || profileLists.includes('1116') || 
        profileLists.includes('1117') || profileLists.includes('1131')) {
          menu += `<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                          aria-expanded="false">
                          แก้ไข-ปิดการนับ-อนุมัติ<span class="caret"></span></a>

                      <ul class="dropdown-menu">`;
          menu += (profileLists.includes('1113') || profileLists.includes('1114') || profileLists.includes('1115')) 
                  ? `<li><a href="javascript:cntModify()">แก้ไข/ปิดการนับ</a></li>` : '';
          menu += (profileLists.includes('1116') || profileLists.includes('1117')) 
                  ? `<li><a href="javascript:cntApprove()">อนุมัติการนับ</a></li>` : '';
          menu += (profileLists.includes('1131')) ? `<li><a href="javascript:cntVoid()">ยกเลิกใบนับ</a></li>` : '';
          menu += `</ul></li>`;
    }

    if (profileLists.includes('1118') || profileLists.includes('1119') ||
        profileLists.includes('1120') ) {
          menu += `<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                          aria-expanded="false">
                          POST<span class="caret"></span></a>

                      <ul class="dropdown-menu">`;
          menu += (profileLists.includes('1120')) ? `<li><a href="javascript:postOrNoPost()">POST</a></li>` : '';
          menu += (profileLists.includes('1119')) ? `<li><a href="javascript:productNoPostManagement()">กำหนดสินค้า ห้าม POST</a></li>` : '';
          menu += (profileLists.includes('1118')) ? `<li><a href="javascript:noPostReason()">เหตุผลไม่ POST</a></li>` : '';
          menu += `</ul></li>`;
    }

    if (profileLists.includes('1121') || profileLists.includes('1122') || 
        profileLists.includes('1123') || profileLists.includes('1124') || 
        profileLists.includes('1125') || profileLists.includes('1126') || 
        profileLists.includes('1127')) {
          menu += `<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                          aria-expanded="false">
                          รายงาน<span class="caret"></span></a>

                      <ul class="dropdown-menu">`;
          menu += (profileLists.includes('1121')) ? `<li><a href="javascript:transactionRep();">Transaction</a></li>` : '';
          menu += (profileLists.includes('1122')) ? `<li><a href="javascript:varianceRep();">Variance Report</a></li>` : '';
          menu += (profileLists.includes('1123')) ? `<li><a href="javascript:noCntRep();">No Count Report</a></li>` : '';
          menu += (profileLists.includes('1124')) ? `<li><a href="javascript:zeroCntRep();">Zero Count Report</a></li>` : '';
          menu += (profileLists.includes('1125')) ? `<li><a href="javascript:statusPRep();">Status P Report</a></li>` : '';
          menu += (profileLists.includes('1126')) ? `<li><a href="javascript:finalCycleCntRep();">Final Cycle Count Report</a></li>` : '';
          menu += (profileLists.includes('1127')) ? `<li><a href="javascript:postRep();">POST Report</a></li>` : '';
          menu += `</ul></li>`;
    }
    

    res.end(menu)
})

module.exports = router;