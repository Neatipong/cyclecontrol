/**
 * to be load with welccome.html
 */
var cashierId = "";
var userGroup = 0;
var oprDate = "";
var storeCode = "";
var lockNumber = "";
var cashierName = "";
var localSrv = "";
var cloudSrv = "";
var waitgif = "<img src='../images/wait.gif'>";
var syserror = " <img src ='../images/error.jpg' width='200px'><br> ";

function userHasLogin() {
	//	var stcode = readCookie('STCODE');
	lockNumber = readCookie('LOCKNO');
	cashierId = readCookie('USERID');
	lockNumber = readCookie('LOCKNO');
	userGroup = readCookie('UGROUP');
	$('#user_login').text(cashierId);
	//---------stcode alway from NODE Server
	$.get('./POSSRV/STINFO', function (res) {
		storeCode = res.stcode;
		localSrv = res.lclsrv;
		cloudSrv = res.cldsrv;
		saveCookie("STCODE", storeCode, 1);
		var htm = " <table id='sysConfig'> " +
			"<tr><td>LocalServer:</td><td>" + localSrv +
			"<tr><td>Cloud Server:</td><td id ='cldServer'>" + cloudSrv +
			"<tr><td>Store:<td>" + storeCode +
			"<tr><td>User ID<td>" + cashierId +
			"<tr><td>User Grp<td>" + userGroup +
			" <tr><td>POS LOCK:<td>" + lockNumber +
			"</table>";
		$('#whoLogin').html(htm);
		currentTime();
	}).fail(function (jqxhr, settings, exception) {
			alert("Can not get Store info ");
		}

	);
}
//-----Current Time
function currentTime() {
	try {
		var nd = new Date;
		var h = addZero(nd.getHours());
		var m = addZero(nd.getMinutes());
		var s = addZero(nd.getSeconds());

		var sec = nd.getSeconds()
		$('#localTime').text(h + ' :' + m + ':' + s);


		if ((sec % 10) == 0) chekCloudSrv(); //1 minute interval
		setTimeout(currentTime, 1000);
	} catch (ex) {
		alert(ex.message);
	}
}

function addZero(x) {
	var xstr = 100 + x * 1;
	xstr += ' ';
	return (xstr.substring(1));
}
//-----Check Cld server
function chekCloudSrv() {
	$.get("./POSSRV/RMCNN", function (data) {
		$('#cldServer').css('background-color', 'lightgreen');
		//---upload UPSTREAM if Any
		///  uploadUpstream();    >>>DO it on seperate Thread !!!!!
	}).fail(function () {
		$('#cldServer').css('background-color', 'crimson');
	});
}

function uploadUpstream() {
	$.get("./POSSRV/UPLOAD", function (data) {
		$('#cldServer').css('background-color', 'lightgreen');
		//---upload UPSTREAM if Any
	}).fail(function () {
		$('#cldServer').css('background-color', 'crimson');
	});
}

function card2login() {
	var card = $('#xidcard').val();
	if (card.length < 10) {
		alert('Invalid Scan IDCARD(require 10 Digit)');
		return;
	}

	var sUrl = "./POSSRV/IDCARD/" + card;
	$.get(sUrl, function (ret) {
		//--ret is jason data 
		var ecode = ret.dbcode;
		var emsg = ret.dbmessage;
		//--ret is jason data 
		var ecode = ret.dbcode;
		var emsg = ret.dbmessage;
		if (ecode > 0) alert('dbError..' + emsg);
		else loginOK(ret.dbitems[0]);
	}).fail(function (jqxhr, settings, exception) {
		alert("Can not call Service " + sUrl);
	});
}

function userLogin() {
	var userid = $('#xuserid').val();
	var passw = $('#passw').val();
	var sUrl = "./POSSRV/LOGIN/" + userid + "/" + passw;

	$.get(sUrl, function (ret) {
		//ret is jason data 
		var jsn = ret; //JSON.parse(ret);
		var ecode = jsn.dbcode;
		var emsg = jsn.dbmessage;
		// alert(emsg);
		if (ecode > 0) alert('dbError..' + emsg);
		else loginOK(ret.dbitems[0]);
	}).fail(function (jqxhr, settings, exception) {
		alert("Can not call Service " + sUrl);
	});

}

function loginClick() {
	//------button is clicked, check for Checkobx
	var cbx = $('#checkbox1').prop('checked');
	if (!cbx) {
		card2login();
	} else {
		userLogin();
	}
}

function loginOK(item) {
	try {
		var ugp = item.USERGROUP; //$(ret).find("USERGROUP").text();
		var uid = item.USERID; // $(ret).find("USERID").text();
		var unm = item.USERNAME; // $(ret).find("USERNAME").text();
		//    var stc 	= item.STCODE;
		var lck = item.LOCKNO;
		//userid=uid;
		//--------Save it in cookie ----
		saveCookie("USERID", uid, 1);
		saveCookie("UGROUP", ugp, 1);
		saveCookie("UNAME", unm, 1);
		//	 saveCookie("STCODE",stc, 1);  //---
		saveCookie("LOCKNO", lck, 1);
		location.reload();
	} catch (ex) {
		alert(ex.message);
	}
}

function showProfile() {
	$('#whoLogin').show();
}


function openSalesX() {
	var url = '/salemenu';
	window.open(url, 'possale', 'toolbar=no, menubar=no,location=no, resizable=no');
}
//------New VErsion 
function openSales() {
	//check logi casher first
	if (cashierId == '' || cashierId == 'undefined') {
		alert('Cashier [' + cashierId + '] is not login yet');
		location.reload();
		return;
	}
	var url = './CTLF?fname=./nsales/salemenu.htm';
	//window.open(url,'possale' );
	window.open(url, 'possale', 'toolbar=no, menubar=no,location=no, resizable=no');
}
//------New VErsion 
function openDetail(pat) {
	var url = './DPAGE?' + btoa(pat);
	//window.open(url,'possale' );
	window.open(url, 'DETAIL', 'toolbar=no, menubar=no,location=no, resizable=no');
}

function openEasy() {
	var url = '/easymenu';
	window.open(url, 'possale', 'toolbar=no, menubar=no,location=no, resizable=no');
}

var currentPat = "";

function openMenu(pat, lv) {
	try {
		if (lv * 100 > userGroup) {
			upfAlert('You dont have Authority to use this menu', function () {
				return;
			});
			return;
		}
		//----------wait ...
		$('#mainDiv').html(waitgif);
		//--------Create Pattern inside
		// pat="./public/"+pat;
		var pdata = {
			patname: pat,
			para: {
				store: storeCode,
				cashier: cashierId,
				lockno: lockNumber
			}
		};
		ajaxXpandFile(pdata, function (bool, ret) {
			if (ret.substring(0, 3) == 'ERR') ret = syserror + ret;

			$('#mainDiv').html(ret);
			//-----Call Startpage
			try {
				pageReady();
			} catch (ex) {
				console.log(ex.message);
			}
			$('#whoLogin').hide();
			currentPat = pat;
		});
	} catch (Ex) {
		alert(ex.message);
	}
}

function pageReload() {
	openMenu(currentPat);
}

function welcome() {
	location.reload();
}

function keyCDPress(ev) {
	if (ev.keyCode == 13) {
		query2login();

	}
}
//---------------------
function logOut() {
	var mdal = $("#myModal");
	$('#mdHeader').html("LOGOUT");
	$("#mdBody").text("Confirm Logout");
	$("#myBtnOK").show();
	$("#myModal").modal();
	$("#myBtnOK").unbind();
	$("#myBtnOK").click(function () {
		query2Logout();
	});

	$("#myBtnCC").unbind();
	$("#myBtnCC").click(function () {
		$("#myModal").modal("hide");
	});
}

function query2Logout() {
	if (storeCode === 'undefined') storeCode = '005';
	var sUrl = "./POSSRV/LOGOUT/" + storeCode + "/" + cashierId + "/" + lockNumber;
	// alert(sUrl);
	$.get(sUrl, function (ret) {
		var ecode = ret.dbcode;
		if (ecode == 0) {
			//--------Clear cookie
			//	removeCookie('USERID');
			//	removeCookie('UNAME');
			//	removeCookie('LOCKNO');
			//   location.reload();
		} else
			alert(ret.dbmessage);
	}).fail(function (jqxhr, settings, exception) {
		alert("Can not call Service " + sUrl);
	});
	//--------Clear cookie
	removeCookie('USERID');
	removeCookie('UNAME');
	removeCookie('LOCKNO');
	location.reload();
}
//------------------------
function upfAlert(text, callBack) {
	$("#mdBody").text(text);
	$('#mdHeader').html("ALert");
	$("#myBtnOK").text("CLOSE");
	$("#myBtnCC").hide();
	$("#myModal").modal('show');
	$("#myBtnOK").click(function () {
		$("#myModal").modal("hide");
		callBack(true);
	});
	setTimeout(function () {
		$("#myModal").modal("hide");
		callBack(true);
	}, 4000);
}

//-------------------------------
function upfConfirm(text, callBack) {
	$("#mdBody").text(text);
	$("#myBtnOK").text("YES");
	$("#myBtnOK").show();
	$("#myBtnCC").show();
	$("#myModal").modal('show');
	$("#myBtnCC").unbind();
	$("#myBtnCC").click(function () {
		$("#myModal").modal("hide");
		callBack(false);
	});
	$("#myBtnOK").unbind(); // ---importance, unbind the old event
	$("#myBtnOK").click(function () {
		$("#myModal").modal("hide");
		callBack(true);
	});
}
//--------------------reset password ----
function resetPassword() {
	var uid = $('#xuserid').val();
	if (uid == '') {
		alert('You must Enter User Id first');
		return;
	}
	//-------------chek email --
	var qry = "Select EMAIL from USERTAB where USERID='" + uid + "'";

	ajaxGetQuery(qry, function (isok, ret) {
		if (isok) {
			var eml = ret.EMAIL;
			var usr64 = btoa(uid);

			if (confirm('Reset Passwrod to this Email [' + eml + ']') == 0) return;

			//-----Send mail to receiper
			// eml='paisan@upfront.co.th';
			var para = 'USERID=' + uid + '&EMAIL=' + eml;
			var pdata = {
				sendto: eml,
				subject: 'Request to change password to UPFPOS',
				mailbody: 'Click to This link http://vpn.upfront.co.th:81/PASSW/' + btoa(para)
			}
			var sUrl = "../EMAIL/SEND";
			try {
				$.ajax({
					type: "POST",
					url: sUrl,
					data: JSON.stringify(pdata),
					dataType: "text",
					contentType: "application/json; charset=UTF-8",
					success: function (ret) { //return is array of data in json 
						alert(ret);
						return;
					},
					error: function (e) {
						alert(e.responseText);
					}
				});
			} catch (ex) {
				callBack(false, ex.message);
			}
		} else alert(qry + '  >>' + ret);
	});
}