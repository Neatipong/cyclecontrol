
exports.msGetQuery = function(sql,callback){
			getQuery(sql,function(ret){
				callback(ret);
			  });
};


const config = require('config');
const dbConfig = config.get('mssqlConfig');
//const dbConfig = config.get('Customer.dbConfig');

function   getQuery(sql,callback){
	//console.log(sql);
	
	var Connection 	= require('tedious').Connection;
	var Request 	= require('tedious').Request; 
	var dbName		= dbConfig.dbName;
	
	var config = {
	    server: dbConfig.host, //"vpn.upfront.co.th",
		char_set:"utf8",
	    // If you're on Windows Azure, you will need this:
	    options:{ database:dbName,
	    		encrypt: true
	    		},
	    authentication: {
			    type: "default",
			    options: {  
			        userName: dbConfig.userid, //"upfdba",
			        password: dbConfig.password //"upfrontadmin"
			      }
				 }
	     }

 var connection = new  Connection(config); 
	connection.on('eror'
      , function(err) {
				console.log("MSSQL Connect err...\n"+err);
			     var resp ={
							dbcode:999,
							dbmessage:err,
					   		dbitems:null
			              }
				callback(resp);
			     return;
	        });
      
  connection.on('connect'
	, function(err) {
		if(err){
			console.log("MSSQL Connect err...\n"+err);
			  var resp ={
						dbcode:999,
						dbmessage:err,
				   		dbitems:null
				   }
				  callback(resp);
			  return;
			}
		
    var results = []; 
    var fields	=[];
    var ecode =0;
    var emsg="OK";
    
    var request = new Request(sql
			,function(err, rowCount) {
			  if (err) {
					ecode=err['number'];
			    	emsg=err['message'];
			    	console.log('error ..'+emsg +' \ndatabase '+dbName);
			  } else {
					console.log('Read db '+ rowCount + ' rows');
				}
			});
    
    	request.on('columnMetadata', function (columns) { 
    		var item = {}; 
			columns.forEach(function (column) { 
				item ={ cname:column.colName,
						ctype:column.type.name,
						clen:column.dataLength
						}
				}); 
				fields.push(item); 
    	});
    	
      request.on("row", function (columns) { 
			var item = {}; 
			columns.forEach(function (column) { 
				item[column.metadata.colName] =column.value
				}); 
				results.push(item); 
		    }); 
      
      request.on("error", function (err) { 
    	  ecode=err['number'];
    	  emsg=err['RequestError'];
    	  console.log('error ..'+emsg +' \ndatabase '+dbName);
		    }); 
      
      request.on('requestCompleted',function(){
    	  var resp ={
 				 dbcode:ecode,
 				 dbmessage:emsg,
 				 dbfileds:fields,
 				 dbitems:results
 	   			}
    	 // console.log(results);
    	  	callback(resp);
      	}); 
      
      connection.execSql(request);
	});
  
  
};

