var dbdate = require('dateformat');
var ms = require('../resource/dbconnect'); 
var xpd = require('../resource/xpand');
var async = require('async');
var bodyParser 	= require('body-parser')
var dt 			= require('../resource/datetime');
var fs 			=require("fs");
var ms			= require('../resource/dbconnect');
var dbdate = require('dateformat');


exports.expandPdata = function(pdata,callback){   //control Json and Result Set
           expandPdata(pdata,function(ret){
               callback(ret);
             });
};

function expandPdata(pdata, callback) {
	// console.log(req.body);
    
	 var rethtm;
	 var addscript=""; //--thids is where we put javascript from script file
	 var ctlstr;
	 var patctl={
			 hdr:null,
			 bdy:null,
			 qry:null,
			 erx:null,
			 cmp:null,
			 pre:null,
			 end:null,
			 xid:"NONE"
	 };
     var xtabs=[];
     
    var fname =pdata.patname;

	//-------------------- 
	 var dir=__dirname 
	 
	  console.log('Expand pattern ' + fname);
	  
	  //---read file synch ---
	  try {
		  ctlstr =fs.readFileSync(fname,'utf-8');
		  
		  //---remove remark
		   ctlstr =removeRemark(ctlstr);
		   
		    //------Remove Script part
	      	if(ctlstr.indexOf("/***")>0){
	      		var scpts=ctlstr.split("/***")[0]; //upper part of $HDR
					addscript="<script>"+scpts+"</script>";
				ctlstr =ctlstr.split("/***")[1];
	      	  }
	      	
	      	 //  Replace parameter 
			 var para	=	 pdata.para;
			 for(key in para){
					 var val =para[key];
					 var rp  =RegExp('@'+key,'g');
					 ctlstr	 =ctlstr.replace(rp,val);
				 }
			 
		    //  Check if any expansion needed
		    if(ctlstr.indexOf('$HDR')<0){
			    callback(addscript + ctlstr); //pass the whole file back
			   
			    return; 
		     }
		    
			  //---split ctlstr int section ---
				 
				var lns=ctlstr.split("$");
				var ntab=0;
				for(var n=1;n<lns.length;n++)
					{
					var str =lns[n];
					if(str.substring(0,3)=="HDR" )patctl.hdr=str.substring(4);
					if(str.substring(0,3)=="SQL" )patctl.qry=str.substring(4);
					if(str.substring(0,3)=="BDY" )patctl.bdy=str.substring(4);
					if(str.substring(0,3)=="END" )patctl.end=str.substring(4);
					if(str.substring(0,3)=="CMP" )patctl.cmp=str.substring(4);
					if(str.substring(0,3)=="ERX" )patctl.erx=str.substring(4);
					if(str.substring(0,3)=="TAB" ){
						xtabs[ntab]=str.substring(4);
						ntab+=1;
						}
					if(str.substring(0,3)=="PRE" )patctl.pre=str.substring(4);
					}

				  
				//console.log('Start asynch');
				
			  //---!!!now call function in asynch Manner !!!
				  async.series([
					    function(callback){ 
						var pres=patctl.pre;
						//-----Xpand PreStatement
						if(pres!=null){
							// console.log('Expand PRE..'+pres);
							ms.dbGetQuery(pres,function(resp){
							     console.log('PRE Qry Return ..' +  resp.dbcode  );
							     //?????it return in object ???
							     
								//---replace hdr,bdy and END with 
								if(resp.dbcode===0){
									for(key in resp.dbitems[0]){
										var val =resp.dbitems[0][key];
										
										try { 
										//----only mssql that reurn type string
										var cty  =resp.dbfields[0][key];
										cty=cty.toLowerCase();          
										if(cty.indexOf('datetime')>0){
											val =dbdate(val,'yyyy-mm-dd HH.MM');
											console.log(key +' '+cty);
										   }
										}
										catch(ex){
											//--do nothing
											//val=val;
										}
										
										var rp	=RegExp(key,'g');
										//console.log('repl '+rp +' w '+val);
										patctl.bdy=patctl.bdy.replace(rp,val);
										patctl.hdr=patctl.hdr.replace(rp,val);
										patctl.qry=patctl.qry.replace(rp,val);
										patctl.end=patctl.end.replace(rp,val);
										//-replace in xtabs
										for(var nt=0;nt<ntab;nt++){
												xtabs[nt]=xtabs[nt].replace(rp,val);
										} 
									}
									callback(null,'XPRE');
								}
								else {
									//console.log('Error in PRE statement');
									rethtm='Error in PRE statement '+resp.dbmessage;
									callback(true,'XPRE'); // no further xpand
								}
							 	
							});
						}
						else	callback(null,'NO PRE');
			        },   
			        //-------------MAIN XPAND
				     function(callback){
				        	console.log('Xpand Main body ' )
				        	xpd.expandCstr(patctl,function(htm){ //control string  + result set
				        		 rethtm=htm ;
				        		 callback(null,"XPN");
				        	});
			 	    } ,
			 	     //-----------for $TAB Xpansion
			        function(callback){
			        // console.log('Exp TAB' );
			         var ntab=xtabs.length;
			         
			         if(ntab>0){
			        		for(var k=0;k<ntab;k++){
			        			xpd.expandOStr(xtabs[k],function(xid,htm){
			        				//console.log(xid +' Xpn Return '+htm)
			        				rethtm =rethtm.replace(xid,htm); 
				        	  		ntab-=1;
				        	  		if(ntab==0){ 
				        	  			callback(null,'XTAB');  //--when all is expanded then go to next step
				        	  		}
			        			});
			        		 
			        		}
			       
			        	}
			         else callback(null,"NO XTAB ");
			        } 
			 	   //-----------------if there is XPNAD inside XPAND
				       , function(callback){
				        	if(rethtm.indexOf('<*')>0){
				        		var insidexpd=[];
				        		var ntab=0;
				        		while(1==1){
				        			var st=rethtm.indexOf('<*');
				        			if(st<0)break;
				        			var en  =rethtm.indexOf('*>',st);
				        	   	insidexpd[ntab]='|XID #inxpd'+ntab+ '\n'+ rethtm.substring(st+2,en);
				        		//	console.log(insidexpd[ntab]);
				        			rethtm=rethtm.substring(0,st)+'#inxpd'+ntab +rethtm.substring(en+3);
				        			ntab++;
				        		}
				        	 
				        		for(var k=0;k<ntab;k++){
				        			xpd.expandOStr(insidexpd[k],function(xid,htm){
				        				rethtm =rethtm.replace(xid,htm); 
					        	  		ntab-=1;
					        	  		if(ntab==0)callback(null,'INSIDE XTAB');
				        			});
				        		}	
				        		 	
				        	}
				        	else callback(null,"NO XPD INSIDE ");
				        }
			 ],
		    function (err, result) {
		   	//	console.log(result);
			    console.log('Script has been added '+addscript.length);
			  //  console.log(rethtm);
			    callback(addscript+rethtm);
			    
			});	 
	
      }
	  
	  catch(ex) {
			console.log('pattern file '+fname  +' Error '+ex.message);
			rethtm='ERR .. Pattern File '+fname +' not Found' ;
		  callback(rethtm);
		   
	  }	
}
  
function removeRemark(data){
	var ret="";
	var strs =data.split("\n");
	for(var i =0;i < strs.length; i++){
			var x =strs[i].trim();
			if (x.substr(0,2)!="//")ret = ret+ x +"\n"; 
	 }
	return(ret);
	
} 
 