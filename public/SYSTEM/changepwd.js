 
 function pageReady(){
        var usrid=readCookie('CCUSERID');
        console.log(usrid)
        if(usrid ==null){
            alert('You are not login yet')
            return;
        }
     $('#usrtxt').val(usrid);
     $('#chPasswModal').modal('show');
 }

 function confirmClick(){
    let re = /^[A-Za-z0-9]{6,}$/;
    let usrid=$('#usrtxt').val();
    let opwd=$('#pwdtxt').val();
    let npwd=$('#nwdtxt').val();
    let cpwd=$('#cwdtxt').val();
    if(npwd.length<6) {
        swalConfirm('warning', 'ไม่สำเร็จ', 'Password must contain More than 6 characters.')
        return;
    }
    if(!re.test(npwd)){
        swalConfirm('warning', 'ไม่สำเร็จ', 'Password must contain only letters, numbers.')
        return;
    }
    re = /[0-9]/;
    if(!re.test(npwd)){
        swalConfirm('warning', 'ไม่สำเร็จ', 'Password must contain at least one number(0-9).')
        return;
    }
    re = /[A-Za-z]/;
    if(!re.test(npwd)){
        swalConfirm('warning', 'ไม่สำเร็จ', 'Password must contain at least 1 character.')
        return
    }
    if(npwd!=cpwd) {
        swalConfirm('warning', 'ไม่สำเร็จ', 'Confirm Passwrod is not the same.')
        return;
    }

    let qry = `Select PASSWORD,OLDPWD1,OLDPWD2,OLDPWD3,
                      [NEWPWD] = dbo.EnCrypt('${usrid}', '${npwd}'),
                      [OLDPWD] = dbo.EnCrypt('${usrid}', '${opwd}')
                From SYSTEMUSER Where USERID= '${usrid}'
                `;
    ajaxGetQuery(qry, (isok, ret) => {
        let jsn = JSON.parse(ret);
        if(jsn.dbcode == 0){
            let oldpwd = jsn.dbitems[0].OLDPWD;
            let pwd = jsn.dbitems[0].PASSWORD;
            let oldpwd1 = jsn.dbitems[0].OLDPWD1;
            let oldpwd2 = jsn.dbitems[0].OLDPWD2;
            let oldpwd3 = jsn.dbitems[0].OLDPWD3;
            let newpwd = jsn.dbitems[0].NEWPWD;
            if (oldpwd != pwd) {
                swalConfirm('warning', 'ไม่สามารถเปลี่ยนรหัสผ่านได้', 'รหัสผ่านเดิมไม่ถูกต้อง กรุณาระบุใหม่อีกครั้ง.')
                return;
            }

            if (newpwd == oldpwd1 || newpwd == oldpwd2 || newpwd == oldpwd3) {
                swalConfirm('warning', 'ไม่สามารถเปลี่ยนรหัสผ่านได้', 'รหัสผ่านใหม่ซ้ำกับรหัสผ่านเก่า 3 ตัวล่าสุดที่เคยใช้มา \r\nกรุณาระบุใหม่อีกครั้ง.')
                return;
            }

            let qry = `update SYSTEMUSER 
                        set PASSWORD = '${newpwd}',
                            INVALIDPWD=0,
                            LASTLOGIN=getdate(),
                            LSTATUS = 'N',
                            CHGPWDDATE = getdate()
                        where USERID = '${usrid}'`;
            let pdata = {
                query: qry
            }
            ajaxExec(pdata, (isok, ret) => {
                let jsn = JSON.parse(ret);
                if(jsn.dbcode == 0) {
                    let qry = `exec WebCC_CheckUserLogin2  '${usrid}','${npwd}',''`;
                    ajaxGetQuery(qry, (isok, ret) => {
                        let jsn = JSON.parse(ret);
                        if(jsn.dbcode == 0) {
                            let now = new Date();
                            let chgpwddate = new Date(jsn.dbitems[0].CHGPWDDATE);
                            const diffTime = Math.abs(chgpwddate - now);
                            let lastChgPwdDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

                            saveCookie('CCUSERID',usrid );
                            saveCookie('CCUGROUP',jsn.dbitems[0].USERGROUP);  
                            saveCookie('CCPRICEORCOST',jsn.dbitems[0].PRICEORCOST );
                            saveCookie('CCGROUPNAME',jsn.dbitems[0].GroupName)
                            saveCookie('CCLSTCHGPWDDAYS', lastChgPwdDays);
                            swalAl('success', 'Changed password successful.');
                            location.reload();
                        } else {
                            swalAl('error', 'Failed', jsn.dbmessage);
                            return;
                        }
                    })
                    
                } else {
                    swalAl('error', 'Failed', jsn.dbmessage);
                    return;
                }
            })
        } else {
            swalAl('error', 'Failed', jsn.dbmessage);
            return;
        }
    })
    // var qry="exec ChangePassword '@userid','@passw','@npassw' ";
    
    // var pdata={query:qry,
    //         para:{userid:usrid,
    //         passw:opwd,
    //         npassw:npwd}}
    // ajaxExec(pdata,function(isok,ret){
    //     if(isok){
    //         var jsn=JSON.parse(ret);
    //         if(jsn.dberror==0){
    //             alert('Password has been changed successful');
    //             location.reload();
    //         }
    //         else alert(jsn.dbmessage);
    //     }
    //     else alert('System Error can not call ajax')
    // });
     
 }
   
/*** 
  
 <div class="modal fade" id="chPasswModal" role="dialog"  tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header" style="padding:25px 20px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4>  <span class="glyphicon glyphicon-lock"></span>Change Password</h4>
                    
                </div>
                <div style="padding:40px 50px;">
 
                        <div class="form-group">
                            <label for="usrtxt"><span class="glyphicon glyphicon-user"></span> UserId</label>
                            <input type="text" class="form-control" name="xuserid" id="usrtxt" value="" disabled='true'>
                        </div>

                        <div class="form-group">
                            <label for="pwdtxt"><span class="glyphicon glyphicon-eye-open"></span>Old Password</label>
                            <input type="password" class="form-control" name="passw" id="pwdtxt" placeholder="Enter password" value="">
                        </div>
 
                 <div class="form-group">
                            <label for="nwdtxt"><span class="glyphicon glyphicon-eye-open"></span>New Password</label>
                            <input type="password" class="form-control" name="passw" id="nwdtxt" placeholder="New  password" value="">
                        </div>

                 <div class="form-group">
                            <label for="cwdtxt"><span class="glyphicon glyphicon-eye-open"></span>Confirm Password</label>
                            <input type="password" class="form-control" name="passw" id="cwdtxt" placeholder="Confirm password" value="">
                 </div>

                 <button type="button" class="btn btn-success btn-block" onclick="confirmClick()">
                            <span class="glyphicon glyphicon-off"></span>Confirm
                </button>

                        <div id='message' class="info info-default">

                        </div>
                   </form>
                </div>

                <div class="modal-footer">
                    <p>Forgot <a href="javascript:resetPassword();">Password?</a></p>
                </div>

            </div>
        </div> 
    </div>

//END **/

